package cn.gjxblog.xa.service;

import cn.gjxblog.xa.dao.one.HouseDao;
import cn.gjxblog.xa.dao.two.House2Dao;
import cn.gjxblog.xa.entity.House;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 13:45
 */
@Service
public class HouseService {

    @Resource
    HouseDao houseDao;

    @Autowired
    House2Dao house2Dao;

    /**
     * 验证分布式事务是否能保证单个数据库的数据一致性。
     */
   @Transactional
    public void addHouse1(){
        House house = new House();
        house.setId(UUID.randomUUID().toString());
        house.setCityCode("123");
        house.setCityName("测试");
        houseDao.addHouse(house);
//     throw new RuntimeException("测试事务回滚");

    }
    /**
     * 验证分布式事务是否能保证单个数据库的数据一致性。
     */
    @Transactional
    public void addHouse2(){
        House house = new House();
        house.setId(UUID.randomUUID().toString());
        house.setCityCode("123");
        house.setCityName("测试");

        house2Dao.addHouse(house);
        throw new RuntimeException("测试事务回滚");
    }


    /**
     * 验证分布式事务是否能保证两个数据库的数据一致性。
     */
    @Transactional
    public void addHouse3(){
        House house = new House();
        house.setId(UUID.randomUUID().toString());
        house.setCityCode("123");
        house.setCityName("测试");
        houseDao.addHouse(house);

        House house2 = new House();
        house2.setId(UUID.randomUUID().toString());
        house2.setCityCode("123");
        house2.setCityName("测试");

        house2Dao.addHouse(house);
//      throw new RuntimeException("测试事务回滚");

    }


}
