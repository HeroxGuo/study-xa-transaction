package cn.gjxblog.xa.dao.two;

import cn.gjxblog.xa.entity.House;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 11:25
 */
public interface House2Dao {

    @Select("select * from city where id = #{id}")
    House getHouse(String id);

    @Insert("insert into city(id,city_name,city_code) values(#{id},#{cityName},#{cityCode})")
    int addHouse(House house);
}
