package cn.gjxblog.xa.controller;

import cn.gjxblog.xa.service.HouseService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 14:25
 */
@RestController
public class TestController {

    @Resource
    private HouseService houseService;

    @RequestMapping("/test")
    public String test(){
        houseService.addHouse3();
        return "ok";
    }
}
