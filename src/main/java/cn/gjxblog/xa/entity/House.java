package cn.gjxblog.xa.entity;

import lombok.Data;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 11:26
 */
@Data
public class House {
    private String id;
    private String cityName;
    private String cityCode;

    @Override
    public String toString() {
        return "House{" +
                "id='" + id + '\'' +
                ", cityName='" + cityName + '\'' +
                ", cityCode='" + cityCode + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }
}
