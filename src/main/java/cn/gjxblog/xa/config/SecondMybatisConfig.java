package cn.gjxblog.xa.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 10:40
 */
@Configuration
@MapperScan(basePackages = SecondMybatisConfig.BASE_PACKAGE,sqlSessionTemplateRef = "sqlSessionTemplateTwo")
public class SecondMybatisConfig {

    //mapper模式下的接口层
    static final String BASE_PACKAGE = "cn.gjxblog.xa.dao.two";

    static final String MAPPER_LOCATION = "classpath:mybatis/two/*.xml";


    @Bean
    public SqlSessionFactory sqlSessionFactoryTwo(@Qualifier("dataSourceTwo") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        factoryBean.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources(MAPPER_LOCATION)
        );

        return factoryBean.getObject();
    }


    @Bean
    public SqlSessionTemplate sqlSessionTemplateTwo(@Qualifier("sqlSessionFactoryTwo") SqlSessionFactory factoryOne){
        SqlSessionTemplate template = new SqlSessionTemplate(factoryOne);
        return  template;
    }

}
