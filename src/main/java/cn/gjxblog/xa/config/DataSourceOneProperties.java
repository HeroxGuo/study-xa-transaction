package cn.gjxblog.xa.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 15:21
 */
@Primary
@Configuration
@ConfigurationProperties("spring.datasource.druid.one")
public class DataSourceOneProperties extends DataSourceProperties {



}
