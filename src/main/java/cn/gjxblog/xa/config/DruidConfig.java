package cn.gjxblog.xa.config;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.sql.XADataSource;
import java.util.Properties;

/**
 * @author gjx
 * @version v1.0
 * @Description
 * @date 2019/7/3 10:23
 */
@Configuration
public class DruidConfig {


    @Primary
    @Bean("dataSourceOne")
    public DataSource dataSourceOne(DataSourceOneProperties dataSourceOneProperties){
        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        ds.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
        Properties oneProperties = new Properties();

        oneProperties.put("url", dataSourceOneProperties.getUrl());
        oneProperties.put("username", dataSourceOneProperties.getUsername());
        oneProperties.put("password", dataSourceOneProperties.getPassword());
        ds.setXaProperties(oneProperties);
        return ds;
    }

    @Bean
    public DataSource dataSourceTwo(DataSourceTwoProperties dataSourceProperties){
        AtomikosDataSourceBean ds = new AtomikosDataSourceBean();
        ds.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
        Properties oneProperties = new Properties();

        oneProperties.put("url", dataSourceProperties.getUrl());
        oneProperties.put("username", dataSourceProperties.getUsername());
        oneProperties.put("password", dataSourceProperties.getPassword());
        ds.setXaProperties(oneProperties);
        return ds;
    }
}
