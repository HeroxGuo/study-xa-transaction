package cn.gjxblog.xa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyXaTransactionApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyXaTransactionApplication.class, args);
    }

}
