package cn.gjxblog.xa;

import cn.gjxblog.xa.dao.one.HouseDao;
import cn.gjxblog.xa.dao.two.House2Dao;
import cn.gjxblog.xa.entity.House;
import cn.gjxblog.xa.service.HouseService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class StudyXaTransactionApplicationTests {

    @Autowired
    HouseDao houseDao;

    @Autowired
    House2Dao house2Dao;

    @Autowired
    HouseService houseService;

    @Test
    public void contextLoads() {
        House house = house2Dao.getHouse("2");
        log.info("house info:{}",house);
    }

    @Test
    public void test_house_service(){
        houseService.addHouse1();
    }


    @Test
    public void test_house_service_add(){
        House house = new House();
        house.setId(UUID.randomUUID().toString());
        house.setCityCode("123");
        house.setCityName("测试");
        houseDao.addHouse(house);
    }
}
